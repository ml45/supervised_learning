# Assignment 1 - Supervised Learning

## About
Timothy Isonio

tisonio3@gatech.edu

CS 7641 - Machine Learning - Spring 2021
Code and extras located at:
https://gitlab.com/ml45/supervised_learning

This is a Markdown document with a .txt extension.

## Environment
In the code repo is the environment.yml needed to replicate the Conda environment I used
for this project. If that does not work, in essence creating an environment with these:

    python>=3.8 numpy scipy scikit-learn matplotlib pandas

should work too. I did this project on a Windows 10 PC running an AMD 8-core/16-thread processor,
so there might be some Windows specific stuff in there. Sometimes I hard-coded 14 jobs
so that I had a core to actually use myself while it ran.

## Running the code
In main.py's main function, there are several lines. Comment and uncomment as appropriate
to produce graphs for these datasets and their corresponding algorithms.

A lot of these I didn't bother to even have the code to save the graphs. I just use PyCharm's SciView to
preview the plt.show()'s, and then copy/paste/screenshot into the analysis report.

A lot of times, I just set a breakpoint at the end of a function and used the Python interactive console to make more graphs
and change more hyperparameters manually. When I remembered to, I printed them to pdf (best way I could find to
preserve the REPL formatting) and included them in the extra/ folder.

## References
* Essentially all of the Scikit-learn docs.
* Plotting learning curve, model validation, etc. code almost verbatim scikit-learn code
* Wine quality dataset from UCI
* Baseball Statcast dataset
* The articles also cited in the report