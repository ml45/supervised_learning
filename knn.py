# https://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html
import matplotlib.pyplot as plt
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import *
from sklearn.metrics import *
from sklearn import neighbors

import plotter

def knn(X, y, dataset_name=""):
    fig, axes = plt.subplots(3, 2, figsize=(10, 15))
    title = "Learning Curves (Naive Bayes)"
    # Cross validation with 100 iterations to get smoother mean test and train
    # score curves, each time with 20% data randomly selected as a validation set.
    cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=0)

    estimator = neighbors.KNeighborsClassifier()
    plotter.plot_LC(estimator, title, X, y, axes=axes[:, 0],
                    cv=cv)

    # title = r"Learning Curves (SVM, RBF kernel, $\gamma=0.001$)"
    # # SVC is more expensive so we do a lower number of CV iterations:
    # cv = ShuffleSplit(n_splits=10, test_size=0.2, random_state=0)
    # estimator = tree.DecisionTreeClassifier()
    # plot_learning_curve.plot_learning_curve(estimator, title, X, y, axes=axes[:, 1],
    #                     cv=cv)

    plt.show()

def knn1(X_train, X_test, y_train, y_test, dataset_name=""):
    default_clf = neighbors.KNeighborsClassifier()
    cv = StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337)
    plotter.plot_LC(default_clf, f"{dataset_name} kNN Default Params",
                    X_train, y_train, cv=cv,
                    fname_prefix=f"{dataset_name}/knn-default-", scoring='f1_macro')
    default_clf.fit(X_train, y_train)
    y_pred = default_clf.predict(X_test)
    print(f1_score(y_test, y_pred, average='macro'))

    param_grid = {'n_neighbors':list(range(1, 101)), 'weights':['uniform','distance'],
                  'metric':["euclidean", "manhattan", "chebyshev", "minkowski"]}
    clf = GridSearchCV(neighbors.KNeighborsClassifier(), param_grid=param_grid,
                       verbose=True, cv=cv,
                       scoring='f1_macro', n_jobs=-1)
    clf.fit(X_train, y_train)
    print(clf.best_params_)

    plotter.plot_LC(clf.best_estimator_, f"{dataset_name} kNN {clf.best_params_}",
                    X_train, y_train, cv=cv,
                    fname_prefix=f"{dataset_name}/knn-gridsearch-", scoring='f1_macro' )
    plt.show()

    #print(clf.cv_results_)
    #print(clf.best_params_)

    hp = "n_neighbors"
    plotter.plot_VC(X_train, y_train, clf.best_estimator_, hp, param_grid[hp],
                    title=f"{dataset_name} kNN for {hp}", scoring='f1_macro')

    plotter.plot_VC(X_test, y_test, clf.best_estimator_, hp, param_grid[hp],
                    title=f"{dataset_name} (TEST) kNN for {hp}", scoring='f1_macro')

    y_pred = clf.predict(X_test)
    print(f1_score(y_test, y_pred, average='macro'))
    plot_confusion_matrix(clf, X_test, y_test, normalize='true')
    plt.show()

    f1_max = float('-inf')
    best_k = 1
    for k in range(1, 101):
        new_params = clf.best_params_
        new_params['n_neighbors'] = k
        new_clf = neighbors.KNeighborsClassifier(**new_params)
        new_clf.fit(X_train, y_train)
        f1 = f1_score(y_test, new_clf.predict(X_test), average='macro')
        if f1 > f1_max:
            f1_max = f1
            best_k = k
        print(f"{k}: {f1}")
    print(f"best k: {best_k}, f1: {f1_max}")