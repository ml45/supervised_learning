
# https://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html
import matplotlib.pyplot as plt
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import *
from sklearn import neural_network
from sklearn.model_selection import *
from sklearn.metrics import *

import numpy as np
import copy
import pickle
import plotter


def nn(X, y, dataset_name=""):
    fig, axes = plt.subplots(3, 2, figsize=(10, 15))
    title = "Learning Curves (Naive Bayes)"
    # Cross validation with 100 iterations to get smoother mean test and train
    # score curves, each time with 20% data randomly selected as a validation set.
    cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=0)

    estimator = neural_network.MLPClassifier()
    plotter.plot_LC(estimator, title, X, y, axes=axes[:, 0],
                    cv=cv)

    # title = r"Learning Curves (SVM, RBF kernel, $\gamma=0.001$)"
    # # SVC is more expensive so we do a lower number of CV iterations:
    # cv = ShuffleSplit(n_splits=10, test_size=0.2, random_state=0)
    # estimator = tree.DecisionTreeClassifier()
    # plot_learning_curve.plot_learning_curve(estimator, title, X, y, axes=axes[:, 1],
    #                     cv=cv)

    plt.show()

def nn1(X_train, X_test, y_train, y_test, dataset_name=""):
    import warnings
    from sklearn.exceptions import ConvergenceWarning
    warnings.filterwarnings('ignore', category=ConvergenceWarning)
    cv = StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337)

    default_clf =neural_network.MLPClassifier(max_iter=10000, random_state=1337)
    default_clf.fit(X_train, y_train)
    y_pred = default_clf.predict(X_test)
    print(f1_score(y_test, y_pred, average='macro'))

    plotter.plot_LC(neural_network.MLPClassifier(max_iter=10000, random_state=1337), f"{dataset_name} NN Default Params",
                    X_train, y_train, cv=cv,
                    fname_prefix=f"{dataset_name}/nn-default-")

    param_grid = {'alpha': 10.0 ** -np.arange(1, 7), 'hidden_layer_sizes':[i for i in range(20, 140, 20)]}

    clf = GridSearchCV(neural_network.MLPClassifier(random_state=1337, max_iter=10000), param_grid=param_grid, verbose=True, cv=cv, n_jobs=14)
    clf.fit(X_train, y_train)
    print(clf.best_params_)
    pickle.dump(clf, open(f"{dataset_name}-nn-grid.clf", "wb"))

    plotter.plot_LC(clf.best_estimator_, f"{dataset_name} NN {clf.best_params_}",
                    X_train, y_train, cv=StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337),
                    fname_prefix=f"{dataset_name}/NNgridsearch-")
    plt.show()

    #print(clf.cv_results_)
    #print(clf.best_params_)

    plotter.plot_VC(X_train, y_train, clf.best_estimator_, "alpha", param_grid["alpha"],
                    title=f"{dataset_name} NN for alpha")

    pass

def nn2(X_train, X_test, y_train, y_test, dataset_name=""):
    clf = pickle.load(open(f"{dataset_name}-nn-grid.clf", 'rb'))
    # param_grid = {'alpha': 10.0 ** -np.arange(-1, 7), 'hidden_layer_sizes':[i for i in range(20, 140, 20)]}
    # plotter.plot_VC_semilog(X_train, y_train, clf.best_estimator_, "alpha", param_grid["alpha"],
    #                 title=f"{dataset_name} NN for alpha")
    # plt.show()

    cv = StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337)
    new_params = copy.deepcopy(clf.best_params_)
    new_params['alpha']=0.1
    new_clf = neural_network.MLPClassifier(max_iter=10000, random_state=1337, **new_params)
    plotter.plot_LC(new_clf,
                    f"{dataset_name} NN {new_params}",
                    X_train, y_train, cv=cv,
                    fname_prefix=f"{dataset_name}/nn-manual-alpha1.0-")
    new_clf.fit(X_train, y_train)
    y_pred = new_clf.predict(X_test)
    print(f1_score(y_test, y_pred, average='macro'))
    plot_confusion_matrix(new_clf, X_test, y_test, normalize='true')
    plt.show()