# https://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import *
from sklearn.metrics import *
import pickle
import numpy as np
import plotter

def svm(X, y, dataset_name=""):
    fig, axes = plt.subplots(3, 2, figsize=(10, 15))
    title = "Learning Curves (Naive Bayes)"
    # Cross validation with 100 iterations to get smoother mean test and train
    # score curves, each time with 20% data randomly selected as a validation set.
    cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=0)

    estimator = GaussianNB()
    plotter.plot_LC(estimator, title, X, y, axes=axes[:, 0],
                    cv=cv, fname_prefix=f"{dataset_name}/svm-gaussianNB-")

    title = r"Learning Curves (SVM, RBF kernel, $\gamma=0.001$)"
    # SVC is more expensive so we do a lower number of CV iterations:
    cv = ShuffleSplit(n_splits=10, test_size=0.2, random_state=0)
    estimator = SVC(gamma=0.001)
    plotter.plot_LC(estimator, title, X, y, axes=axes[:, 1],
                    cv=cv, fname_prefix=f"{dataset_name}/svm-svc-gamma=0.001-")

    plt.show()


def svm1(X_train, X_test, y_train, y_test, dataset_name="", kernel="linear"):
    print(f"\nSVM-{kernel}")
    default_clf = SVC(random_state=1337, kernel=kernel, cache_size=1000)
    plotter.plot_LC(default_clf, f"{dataset_name} SVM-{kernel} Default Params",
                    X_train, y_train, cv=StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337),
                    fname_prefix=f"{dataset_name}/svm-{kernel}-default-", scoring='f1_macro')
    default_clf.fit(X_train, y_train)
    y_pred = default_clf.predict(X_test)
    print(f1_score(y_test, y_pred, average='macro'))

    param_grid = {'C':np.exp2(range(-7,5)).tolist()}
    if kernel == "poly":
        param_grid['degree'] = list(range(1,11))
    if kernel in ["rbf", "poly", "sigmoid"]:
        param_grid['gamma'] = ['scale','auto']

    clf = GridSearchCV(SVC(random_state=1337, kernel=kernel, cache_size=1000), param_grid=param_grid,
                       verbose=True, cv=StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337),
                       scoring='f1_macro', n_jobs=14)
    clf.fit(X_train, y_train)
    print(clf.best_params_)
    pickle.dump(clf, open(f"{dataset_name}-svc-{kernel}-grid.clf", "wb"))


    plotter.plot_LC(clf.best_estimator_, f"{dataset_name} SVM-{kernel} {clf.best_params_}",
                    X_train, y_train, cv=StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337),
                    fname_prefix=f"{dataset_name}/svm-{kernel}-gridsearch-", scoring='f1_macro' )
    plt.show()

    #print(clf.cv_results_)
    #print(clf.best_params_)

    hp = "C"
    plotter.plot_VC_semilog(X_train, y_train, clf.best_estimator_, hp, param_grid[hp],
                    title=f"{dataset_name} SVM-{kernel} for {hp}", scoring='f1_macro')

    plotter.plot_VC_semilog(X_test, y_test, clf.best_estimator_, hp, param_grid[hp],
                    title=f"{dataset_name} (TEST) SVM-{kernel} for {hp}", scoring='f1_macro')

    y_pred = clf.predict(X_test)
    print(f1_score(y_test, y_pred, average='macro'))
    plot_confusion_matrix(clf, X_test, y_test, normalize='true')
    plt.show()

    # for max_depth in [7, 16]:
    #     new_params = clf.best_params_
    #     new_params['max_depth'] = max_depth
    #     new_params['class_weight'] = "balanced"
    #     new_params['random_state'] = 1337
    #     new_clf = tree.DecisionTreeClassifier(**new_params)
    #     new_clf.fit(X_train, y_train)
    #     print(f"{max_depth}: {f1_score(y_test, new_clf.predict(X_test), average='macro')}")
    #     plot_confusion_matrix(new_clf, X_test, y_test, normalize='true')
    #     plt.show()
    pass

def svm_tweak_kernels(X_train, X_test, y_train, y_test, dataset_name="", kernel="linear"):
    if kernel == 'linear':
        clf = pickle.load(open("wine-svc-linear-grid.clf", 'rb'))
        new_params = clf.best_params_
        new_params['C'] = 8
        new_clf = SVC(random_state=1337, **new_params)
        new_clf.fit(X_train, y_train)
        plotter.plot_LC(new_clf, f"{dataset_name} SVM-{kernel} {new_params}",
                        X_train, y_train, cv=StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337),
                        fname_prefix=f"{dataset_name}/svm-{kernel}-manual-C8", scoring='f1_macro')
        y_pred = new_clf.predict(X_test)
        print(f1_score(y_test, y_pred, average='macro'))
        plot_confusion_matrix(new_clf, X_test, y_test, normalize='true')
        plt.show()
    elif kernel == 'poly':
        clf = pickle.load(open("wine-svc-poly-grid.clf", 'rb'))
        param_grid = {'C': list(range(96, 161))}
        param_grid['degree'] = list(range(1, 11))

        hp = 'degree'

        new_params = clf.best_params_
        new_clf = SVC(random_state=1337, **new_params)
        new_clf.fit(X_train, y_train)
        plotter.plot_VC(X_train, y_train, new_clf, hp, param_grid[hp],
                                title=f"{dataset_name} SVM-{kernel} {new_params} for {hp}", scoring='f1_macro')
        y_pred = new_clf.predict(X_test)
        print(f1_score(y_test, y_pred, average='macro'))
        plot_confusion_matrix(new_clf, X_test, y_test, normalize='true')
        plt.show()

        pass

def svm_tweak_kernels1(X_train, X_test, y_train, y_test, dataset_name="", kernel="linear"):
    if kernel == 'poly':
        clf = pickle.load(open("wine-svc-poly-grid.clf", 'rb'))
        new_params = clf.best_params_
        new_params['C'] = 16
        new_params['degree'] =2
        new_clf = SVC(random_state=1337, **new_params)
        new_clf.fit(X_train, y_train)
        plotter.plot_LC(new_clf, f"{dataset_name} SVM-{kernel} {new_params}",
                        X_train, y_train, cv=StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337),
                        fname_prefix=f"{dataset_name}/svm-{kernel}-manual-C16", scoring='f1_macro')
        y_pred = new_clf.predict(X_test)
        print(f1_score(y_test, y_pred, average='macro'))
        plot_confusion_matrix(new_clf, X_test, y_test, normalize='true')
        plt.show()
    # elif kernel == 'poly':
    #     clf = pickle.load(open("wine-svc-poly-grid.clf", 'rb'))
    #     param_grid = {'C': list(range(96, 161))}
    #     param_grid['degree'] = list(range(1, 11))
    #
    #     hp = 'degree'
    #
    #     new_params = clf.best_params_
    #     new_clf = SVC(random_state=1337, **new_params)
    #     new_clf.fit(X_train, y_train)
    #     plotter.plot_VC(X_train, y_train, new_clf, hp, param_grid[hp],
    #                             title=f"{dataset_name} SVM-{kernel} {new_params} for {hp}", scoring='f1_macro')
    #     y_pred = new_clf.predict(X_test)
    #     print(f1_score(y_test, y_pred, average='macro'))
    #     plot_confusion_matrix(new_clf, X_test, y_test, normalize='true')
    #     plt.show()
    #
    #     pass