from sklearn import tree

# https://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import *
from sklearn.metrics import *
import plotter
import plot_validation_curve


def dt(X, y, dataset_name=""):

    # Cross validation with 100 iterations to get smoother mean test and train
    # score curves, each time with 20% data randomly selected as a validation set.
    cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=0)

    for max_depth in range(1, 32):
        fig, axes = plt.subplots(3, 2, figsize=(10, 15))
        title = "Learning Curves (DT)"
        estimator = tree.DecisionTreeClassifier(max_depth=max_depth)
        plotter.plot_LC(estimator, title, X, y, axes=axes[:, 0],
                        cv=cv, fname_prefix=f"{dataset_name}/dt-max_depth={max_depth}-")

    # title = r"Learning Curves (SVM, RBF kernel, $\gamma=0.001$)"
    # # SVC is more expensive so we do a lower number of CV iterations:
    # cv = ShuffleSplit(n_splits=10, test_size=0.2, random_state=0)
    # estimator = tree.DecisionTreeClassifier()
    # plot_learning_curve.plot_learning_curve(estimator, title, X, y, axes=axes[:, 1],
    #                     cv=cv)

    plt.show()

def dt1(X_train, X_test, y_train, y_test, dataset_name=""):
    default_clf = tree.DecisionTreeClassifier(class_weight='balanced')
    plotter.plot_LC(default_clf, f"{dataset_name} DT Default Params",
                    X_train, y_train, cv=StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337),
                    fname_prefix=f"{dataset_name}/dt-default-", scoring='f1_macro')
    default_clf.fit(X_train, y_train)
    y_pred = default_clf.predict(X_test)
    print(f1_score(y_test, y_pred, average='macro'))

    param_grid = {'max_depth': list(range(1, 32)), 'max_leaf_nodes': list(range(10, 1000, 10)),
                  'criterion': ['gini', 'entropy']}

    param_grid = {'max_depth': list(range(1, 32)),
                  'criterion': ['gini', 'entropy'],
                  'min_samples_leaf':list(range(1,10)),
                  'max_leaf_nodes':list(range(2,1002,100))}
    param_grid = {'max_depth': list(range(1, 32)),
                  'criterion': ['gini', 'entropy']}

    cv = StratifiedKFold(5, random_state=1337)
    clf = GridSearchCV(tree.DecisionTreeClassifier(random_state=1337,class_weight='balanced'), param_grid=param_grid,
                       verbose=True, cv=StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337),
                       scoring='f1_macro', n_jobs=-1)
    clf.fit(X_train, y_train)
    print(clf.best_params_)

    plotter.plot_LC(clf.best_estimator_, f"{dataset_name} DT {clf.best_params_}",
                    X_train, y_train, cv=StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337),
                    fname_prefix=f"{dataset_name}/dt-gridsearch-", scoring='f1_macro' )
    plt.show()

    #print(clf.cv_results_)
    #print(clf.best_params_)

    hp = "max_depth"
    plotter.plot_VC(X_train, y_train, clf.best_estimator_, hp, param_grid[hp],
                    title=f"{dataset_name} DT for {hp}", scoring='f1_macro')

    plotter.plot_VC(X_test, y_test, clf.best_estimator_, hp, param_grid[hp],
                    title=f"{dataset_name} (TEST) DT for {hp}", scoring='f1_macro')

    y_pred = clf.predict(X_test)
    print(f1_score(y_test, y_pred, average='macro'))
    plot_confusion_matrix(clf, X_test, y_test, normalize='true')
    plt.show()

    for max_depth in [7, 16]:
        new_params = clf.best_params_
        new_params['max_depth'] = max_depth
        new_params['class_weight'] = "balanced"
        new_params['random_state'] = 1337
        new_clf = tree.DecisionTreeClassifier(**new_params)
        new_clf.fit(X_train, y_train)
        print(f"{max_depth}: {f1_score(y_test, new_clf.predict(X_test), average='macro')}")
        plot_confusion_matrix(new_clf, X_test, y_test, normalize='true')
        plt.show()
    pass