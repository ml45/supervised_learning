# https://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html
import matplotlib.pyplot as plt
from sklearn.model_selection import ShuffleSplit
from sklearn import ensemble
from sklearn.model_selection import *
from sklearn.metrics import *
import numpy as np
import pickle
import plotter

def boosting(X, y, dataset_name=""):
    fig, axes = plt.subplots(3, 2, figsize=(10, 15))
    title = "Learning Curves (Naive Bayes)"
    # Cross validation with 100 iterations to get smoother mean test and train
    # score curves, each time with 20% data randomly selected as a validation set.
    cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=0)

    estimator = ensemble.AdaBoostClassifier()
    plotter.plot_LC(estimator, title, X, y, axes=axes[:, 0],
                    cv=cv)

    title = r"Learning Curves (SVM, RBF kernel, $\gamma=0.001$)"
    # SVC is more expensive so we do a lower number of CV iterations:
    cv = ShuffleSplit(n_splits=10, test_size=0.2, random_state=0)
    estimator = ensemble.AdaBoostClassifier()
    plotter.plot_LC(estimator, title, X, y, axes=axes[:, 1],
                    cv=cv)

    plt.show()

def boosting1(X_train, X_test, y_train, y_test, dataset_name=""):
    #default_clf = tree.DecisionTreeClassifier(class_weight='balanced')
    default_clf = ensemble.AdaBoostClassifier(random_state=1337)
    cv = StratifiedShuffleSplit(n_splits=20, test_size=0.2, random_state=1337)

    plotter.plot_LC(default_clf, f"{dataset_name} AdaBoost Default Params",
                    X_train, y_train, cv=cv,
                    fname_prefix=f"{dataset_name}/adaboost-default-", scoring='f1_macro')
    default_clf.fit(X_train, y_train)
    y_pred = default_clf.predict(X_test)
    print(f1_score(y_test, y_pred, average='macro'))

    param_grid = {'n_estimators':list(range(1, 102)), "learning_rate":np.linspace(0.01, 1.0, num=20)}
    clf = GridSearchCV(ensemble.AdaBoostClassifier(random_state=1337), param_grid=param_grid,
                       verbose=True, cv=cv,
                       scoring='f1_macro', n_jobs=14)
    clf.fit(X_train, y_train)
    print(clf.best_params_)

    plotter.plot_LC(clf.best_estimator_, f"{dataset_name} AdaBoost {clf.best_params_}",
                    X_train, y_train, cv=cv,
                    fname_prefix=f"{dataset_name}/adaboost-gridsearch-", scoring='f1_macro' )
    plt.show()

    #print(clf.cv_results_)
    #print(clf.best_params_)

    hp = "n_estimators"
    plotter.plot_VC(X_train, y_train, clf.best_estimator_, hp, param_grid[hp],
                    title=f"{dataset_name} AdaBoost for {hp}", scoring='f1_macro')

    plotter.plot_VC(X_test, y_test, clf.best_estimator_, hp, param_grid[hp],
                    title=f"{dataset_name} (TEST) AdaBoost for {hp}", scoring='f1_macro')

    hp = "learning_rate"
    plotter.plot_VC(X_train, y_train, clf.best_estimator_, hp, param_grid[hp],
                    title=f"{dataset_name} AdaBoost for {hp}", scoring='f1_macro')

    plotter.plot_VC(X_test, y_test, clf.best_estimator_, hp, param_grid[hp],
                    title=f"{dataset_name} (TEST) AdaBoost for {hp}", scoring='f1_macro')

    y_pred = clf.predict(X_test)
    print(f1_score(y_test, y_pred, average='macro'))
    plot_confusion_matrix(clf, X_test, y_test, normalize='true')
    plt.show()

    # for max_depth in range(1, 32):
    #     new_params = clf.best_params_
    #     new_params['max_depth'] = max_depth
    #     new_params['class_weight'] = "balanced"
    #     new_params['random_state'] = 1337
    #     new_clf = tree.DecisionTreeClassifier(**new_params)
    #     new_clf.fit(X_train, y_train)
    #     print(f"{max_depth}: {f1_score(y_test, new_clf.predict(X_test), average='macro')}")
    #     # plot_confusion_matrix(new_clf, X_test, y_test, normalize='true')
    #     # plt.show()
    pass
