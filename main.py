import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.compose import ColumnTransformer
from sklearn.datasets import fetch_openml
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer

import dt
import nn
import boosting
import svm
import knn

def preprocess_dodgers():
    dataset1_fname = "dodgers.csv"
    dataset1 = pd.read_csv(dataset1_fname)

    # dataset1.drop(["game_date", "player_name", "batter", "events", "spin_dir", "spin_rate_deprecated", "break_angle_deprecated",
    #                "zone", "des", "stand", "home_team","away_team","type","hit_location"])
    dataset1 = dataset1[["pitch_type", "release_speed", "release_pos_x", "release_pos_z", "description", "p_throws",
                         "balls", "strikes",
                         "pfx_x", "pfx_z","vx0","vy0","vz0","ax","ay","az","sz_top","sz_bot",
                         "effective_speed", "release_spin_rate", "release_pos_y", "at_bat_number", "pitch_number",
                         "plate_x", "plate_z"]]

    dataset1 = dataset1.dropna()

    # https://towardsdatascience.com/categorical-encoding-using-label-encoding-and-one-hot-encoder-911ef77fb5bd
    dataset1_to1hot = ["pitch_type", "p_throws"]
    enc = OneHotEncoder()
    enc_df = pd.DataFrame(enc.fit_transform(dataset1[dataset1_to1hot]).toarray())
    dataset1 = dataset1.join(enc_df)
    dataset1 = dataset1.drop(["pitch_type", "p_throws"], axis=1)
    dataset1 = dataset1.dropna()

    dataset1_X = dataset1.drop(["description"], axis=1)
    #dataset1_X = enc.fit_transform(dataset1_X)

    dataset1_y = dataset1["description"]

    X_train, X_test, y_train, y_test = train_test_split(dataset1_X, dataset1_y, test_size=0.2, random_state=1337)
    scaler = StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)

    scaler.fit(X_test)
    X_test = scaler.transform(X_test)
    return X_train, X_test, y_train, y_test

def preprocess_wine():
    dataset = pd.read_csv("winequality-red.csv", delimiter=";", header=0)
    dataset = dataset.dropna()
    dataset_X = dataset.drop(["quality"], axis=1)
    dataset_y = dataset["quality"]

    X_train, X_test, y_train, y_test = train_test_split(dataset_X, dataset_y, test_size = 0.2, random_state=1337)

    scaler = StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)

    scaler.fit(X_test)
    X_test = scaler.transform(X_test)


    return X_train, X_test, y_train, y_test

if __name__ == '__main__':
    np.random.seed(1337)

    X_train, X_test, y_train, y_test = preprocess_wine()
    #dt.dt1(X_train, X_test, y_train, y_test, "wine")
    #nn.nn1(X_train, X_test, y_train, y_test, "wine")
    #nn.nn2(X_train, X_test, y_train, y_test, "wine")
    #boosting.boosting1(X_train, X_test, y_train, y_test, "wine")
    # kernels = ['linear', 'poly', 'rbf', 'sigmoid']
    # for kernel in kernels:
    #     svm.svm1(X_train, X_test, y_train, y_test, "wine", kernel)
    #svm.svm_tweak_kernels(X_train, X_test, y_train, y_test, "wine", "linear")
    #svm.svm_tweak_kernels(X_train, X_test, y_train, y_test, "wine", "poly")
    #knn.knn1(X_train, X_test, y_train, y_test, "wine")


    #X_train, X_test, y_train, y_test = preprocess_dodgers()
    #dt.dt1(X_train, X_test, y_train, y_test, "dodgers")
    #nn.nn1(X_train, X_test, y_train, y_test, "dodgers")
    #nn.nn2(X_train, X_test, y_train, y_test, "dodgers")
    #boosting.boosting1(X_train, X_test, y_train, y_test, "dodgers")
    # kernels = ['linear', 'poly', 'rbf', 'sigmoid']
    # for kernel in kernels:
    #     svm.svm1(X_train, X_test, y_train, y_test, "dodgers", kernel)
    # svm.svm_tweak_kernels1(X_train, X_test, y_train, y_test, "dodgers", "linear")
    # svm.svm_tweak_kernels1(X_train, X_test, y_train, y_test, "dodgers", "poly")
    #knn.knn1(X_train, X_test, y_train, y_test, "dodgers")

    pass